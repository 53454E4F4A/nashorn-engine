import java.io.File;
import java.io.FileReader;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.Arrays;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.sun.phobos.script.javascript.RhinoScriptEngineFactory;

import jdk.nashorn.api.scripting.NashornScriptEngineFactory;


public class Main {
 
	public static void main(String[] args) throws Exception {      	
		
		List<GarbageCollectorMXBean> gcmxb = ManagementFactory.getGarbageCollectorMXBeans();
	
        List<String> benchmarkFiles = Arrays.asList(
	        	 "../Octane Benchmarks/base.js",
	        	 "../Octane Benchmarks/richards.js",
	         //    "../Octane Benchmarks/deltablue.js",
	             "../Octane Benchmarks/crypto.js",
	             "../Octane Benchmarks/raytrace.js",
	             "../Octane Benchmarks/earley-boyer.js",
	             "../Octane Benchmarks/regexp.js",
	             "../Octane Benchmarks/splay.js",
	             "../Octane Benchmarks/navier-stokes.js",
	        //     "../Octane Benchmarks/pdfjs.js",
	        //     "../Octane Benchmarks/mandreel.js",
	             "../Octane Benchmarks/gbemu-part1.js",
	             "../Octane Benchmarks/gbemu-part2.js",
	             "../Octane Benchmarks/code-load.js",
	             "../Octane Benchmarks/box2d.js",
	             "../Octane Benchmarks/zlib.js",
	             "../Octane Benchmarks/zlib-data.js",
	             "../Octane Benchmarks/typescript.js",
	             "../Octane Benchmarks/typescript-input.js",
	             "../Octane Benchmarks/typescript-compiler.js");
    
        try {
       	
        	     NashornScriptEngineFactory nashornFactory = new NashornScriptEngineFactory();
                 ScriptEngine engine = 
                		 nashornFactory.getScriptEngine(new String[] { ""}); //
        	
                //s engine.eval(new FileReader("../Octane Benchmarks/run.js"));
          		//ScriptEngineManager factory = new ScriptEngineManager();        		
          	
             	Logger logger = new Logger("./logs/Run 8/"); 
	    		logger.enableGCLogging();
	    		
	    		logger.setFile("nashorn.csv");
	    		logger.setGCLogFile("nashorn-gc.csv");	    
                //ScriptEngine engine = factory.getEngineByName("nashorn"); 
                
              //  JSBenchmark.runAllBenchmarks(engine, benchmarkFiles, 100,logger);
                JSBenchmark.runBenchmarks(engine, benchmarkFiles, 100, logger, 1);
                
                RhinoScriptEngineFactory rhinoFactory = new RhinoScriptEngineFactory();
                engine = 
                		rhinoFactory.getScriptEngine();
                //engine = factory.getEngineByName("rhino"); 
                logger.setFile("rhino.csv");
                logger.setGCLogFile("rhino-gc.csv");
                JSBenchmark.runBenchmarks(engine, benchmarkFiles, 100, logger, 1);
                //JSBenchmark.runAllBenchmarks(engine, benchmarkFiles, 100,logger);      
                
                logger.close();
        } catch (final ScriptException | org.mozilla.javascript.EvaluatorException se) { se.printStackTrace(); }
    }

}
