import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.management.Notification;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;
import javax.management.openmbean.CompositeData;

import com.sun.management.GarbageCollectionNotificationInfo;
/**
 * 
 * 
 * @author Jonas Sch�ufler
 * 
*/
public class Logger implements NotificationListener {

	private PrintWriter writer = null;
	private PrintWriter benchmarkWriter = null;
	private PrintWriter gcLogWriter = null;
	private int mode = FILE;
	public final static int STDOUT = 1;
	public final static int FILE = 2;
	private long runGcDuration = 0;
	private long totalGcDuration = 0;
	private long start = 0;
	private String logDir = "";
	
	public Logger(String dir) {
		start = System.currentTimeMillis();
		logDir = dir;
		// TODO Auto-generated constructor stub
	}

	public void enableGCLogging() {
	
		List<GarbageCollectorMXBean> gcbeans = ManagementFactory
				.getGarbageCollectorMXBeans();

		for (GarbageCollectorMXBean gcbean : gcbeans) {

			NotificationEmitter emitter = (NotificationEmitter) gcbean;
			emitter.addNotificationListener(this, null, null);

		}

		// Add the listener

	}

	public void setGCLogFile(String logFile) {
		try {
			if (this.gcLogWriter != null) {
				this.gcLogWriter.close();
				this.gcLogWriter = null;
			}

			this.gcLogWriter = new PrintWriter(new BufferedWriter(
					new FileWriter(logDir+logFile, true)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setFile(String logFile) {
		try {
			if (this.writer != null) {
				this.writer.close();
				this.writer = null;
			}

			this.writer = new PrintWriter(new BufferedWriter(new FileWriter(logDir+logFile, true)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setBenchmarkLogFile(String logFile) {
		try {
			if (this.benchmarkWriter != null) {
				this.benchmarkWriter.close();
				this.benchmarkWriter = null;
			}

			this.benchmarkWriter = new PrintWriter(new BufferedWriter(new FileWriter(logDir+logFile, true)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void log(String s) {
		switch (mode) {
		case STDOUT:
			System.out.println(System.currentTimeMillis()-start +" "+s);
			break;
		case FILE:
			writer.println(System.currentTimeMillis()-start +" "+s);
			break;
		}

	}
	void logBenchmark(String s) {
		switch (mode) {
		case STDOUT:
			System.out.println(System.currentTimeMillis()-start +" "+s);
			break;
		case FILE:
			benchmarkWriter.println(System.currentTimeMillis()-start +" "+s);
			break;
		}

	}

	void logGC(String s) {
		switch (mode) {
		case STDOUT:
			System.out.println(System.currentTimeMillis()-start +" "+s);
			break;
		case FILE:
			this.gcLogWriter.println(System.currentTimeMillis()-start +" "+s);
			break;
		}

	}
	public long resetGcDuration() {
		long tmp = runGcDuration;
		runGcDuration = 0;
		return tmp;
	}
	public void handleNotification(Notification notification, Object handback) {
		if (notification
				.getType()
				.equals(GarbageCollectionNotificationInfo.GARBAGE_COLLECTION_NOTIFICATION)) {
			GarbageCollectionNotificationInfo info = GarbageCollectionNotificationInfo
					.from((CompositeData) notification.getUserData());

			logGC(info.getGcInfo().getStartTime() + " "
					+ info.getGcInfo().getEndTime());
			
			
			runGcDuration += (info.getGcInfo().getEndTime()-info.getGcInfo().getStartTime());
			// printGCInfo(info);
		}
	}

	public void close() {
		if (this.gcLogWriter != null) {
			this.gcLogWriter.close();
			this.gcLogWriter = null;
		}
		if (this.writer != null) {
			this.writer.close();
			this.writer = null;
		}
		if (this.benchmarkWriter != null) {
			this.benchmarkWriter.close();
			this.benchmarkWriter = null;
		}
	}

	
	/*
	 * method by 
	 */
	@SuppressWarnings("unused")
	private void printGCInfo(GarbageCollectionNotificationInfo info) {

		long duration = info.getGcInfo().getDuration();
		String gctype = info.getGcAction();
		if ("end of minor GC".equals(gctype)) {
			gctype = "Young Gen GC";
		} else if ("end of major GC".equals(gctype)) {
			gctype = "Old Gen GC";
		}

		// System.out.println("GcInfo CompositeType: " +
		// info.getGcInfo().getCompositeType());
		// System.out.println("GcInfo MemoryUsageAfterGc: " +
		// info.getGcInfo().getMemoryUsageAfterGc());
		// System.out.println("GcInfo MemoryUsageBeforeGc: " +
		// info.getGcInfo().getMemoryUsageBeforeGc());
		Map<String, MemoryUsage> membefore = info.getGcInfo()
				.getMemoryUsageBeforeGc();
		Map<String, MemoryUsage> mem = info.getGcInfo().getMemoryUsageAfterGc();
		System.out.println();
		System.out.println(gctype + ": - " + info.getGcInfo().getId() + " "
				+ info.getGcName() + " (from " + info.getGcCause() + ") "
				+ duration + " microseconds; start-end times "
				+ info.getGcInfo().getStartTime() + "-"
				+ info.getGcInfo().getEndTime());
		for (Entry<String, MemoryUsage> entry : mem.entrySet()) {
			String name = entry.getKey();
			MemoryUsage memdetail = entry.getValue();
			long memInit = memdetail.getInit();
			long memCommitted = memdetail.getCommitted();
			long memMax = memdetail.getMax();
			long memUsed = memdetail.getUsed();
			MemoryUsage before = membefore.get(name);
			long beforepercent = ((before.getUsed() * 1000L) / before
					.getCommitted());
			long percent = ((memUsed * 1000L) / before.getCommitted()); // >100%
																		// when
																		// it
																		// gets
																		// expanded

			System.out.println(name
					+ (memCommitted == memMax ? "(fully expanded)"
							: "(still expandable)") + "used: "
					+ (beforepercent / 10) + "." + (beforepercent % 10) + "%->"
					+ (percent / 10) + "." + (percent % 10) + "%("
					+ ((memUsed / 1048576) + 1) + "MB) / ");
		}

		totalGcDuration += info.getGcInfo().getDuration();
		long percent = totalGcDuration * 1000L / info.getGcInfo().getEndTime();
		System.out.println("GC cumulated overhead " + (percent / 10) + "."
				+ (percent % 10) + "%");

	}

}
