import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

/**
 * 
 * Eine Klasse zur Laufzeitmessung von JavaScript.
 * @author Jonas Sch�ufler
 * 
*/

public class JSBenchmark {
	long stamp;
	String script;
	ScriptEngine engine;
	Logger logger;

	class Stamp {
		long stamp;
		long gc_duration;

		Stamp(long s, long gc) {
			stamp = s;
			gc_duration = gc;
		}
	}

	public JSBenchmark(String script, ScriptEngine engine, Logger logger) {

		try {
			this.logger = logger;
			this.engine = engine;
			this.script = readFile(script);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}

	static void runBenchmarks(ScriptEngine engine, List<String> benchmarkFiles,
			int n, Logger logger, int loops) throws ScriptException {

		for (int l = 0; l < loops; l++) {
			Iterator<String> i = benchmarkFiles.iterator();
			while (i.hasNext()) {
				String script = i.next();
				String fileName = script.substring(script.lastIndexOf("/") + 1,
						script.lastIndexOf("."))
						+ "-"
						+ engine.getFactory().getEngineName() + ".log";
				logger.setBenchmarkLogFile(fileName);
				JSBenchmark bm = new JSBenchmark(script, engine, logger);
				bm.runBenchmark(n);
			}
		}
	}

	Stamp stamp() {
		long d = System.currentTimeMillis() - stamp;
		stamp = System.currentTimeMillis();
		return new Stamp(d, logger.resetGcDuration());
	}

	void runBenchmark() throws ScriptException {
		stamp();
		this.engine.eval(this.script);
		Stamp s = stamp();
		String logString = "" + s.stamp + " " + s.gc_duration + " "
				+ (s.stamp - s.gc_duration);
		this.logger.log(logString);
		this.logger.logBenchmark(logString);
	}

	void runBenchmark(int n) throws ScriptException {
		for (int i = 0; i < n; i++) {
			runBenchmark();
		}
	}

}
