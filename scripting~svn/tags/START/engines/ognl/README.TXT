This is JSR-223 script engine for OGNL - Object Graph Navigation Language.
OGNL is available for download at http://www.ognl.org/ The script engine license is 
in LICENSE.TXT. OGNL license is in THIRDPARTYLICENSE.TXT.

