This is JSR-223 script engine for Jacl. Jacl is Java implementation of Tcl (Tool Command Language).
This is available for download at http://tcljava.sourceforge.net/
The script engine license is in LICENSE.TXT. Jacl license is in THIRDPARTYLICENSE.TXT.

