This is JSR-223 script engine for JRuby - Java implementation of Ruby language.
JRuby is available for download at http://jruby.sourceforge.net/.
The script engine license is in LICENSE.TXT. JRuby license is in THIRDPARTYLICENSE.TXT.

