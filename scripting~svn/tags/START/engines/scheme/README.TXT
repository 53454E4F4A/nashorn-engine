This is JSR-223 script engine for SISC - Java implementation of Scheme language.
SISC is available for download at http://sisc.sourceforge.net/.
The script engine license is in LICENSE.TXT. SISC license is in THIRDPARTYLICENSE.TXT.

