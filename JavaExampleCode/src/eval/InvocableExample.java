package eval;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class InvocableExample {
	
	public static void main(String[] args) throws Exception {
      
        ScriptEngineManager factory = new ScriptEngineManager();
       
        ScriptEngine engine = factory.getEngineByName("nashorn"); 
        try {
engine.eval("function hello(args) "
	+ "{ "
	+ 	"return 'hello ' + args + '!' "
	+ "};");  
Invocable invocable = (Invocable) engine;
System.out.println(invocable.invokeFunction("hello", "HAW").getClass()); 
        } catch (final ScriptException se) { se.printStackTrace(); }
    }
}

