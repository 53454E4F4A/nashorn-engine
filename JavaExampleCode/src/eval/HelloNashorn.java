package eval;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class HelloNashorn {
	public static void main(String[] args) throws Exception {
        		
ScriptEngineManager manager = new ScriptEngineManager();        
ScriptEngine engine = manager.getEngineByName("js"); 
try {
	System.out.println(engine.getFactory().getEngineName());
	engine.eval(
		  "function hello(args) "
		+ "{ "
		+ 	"print('hello ' + args + '!') "
		+ "};"
	);
	engine.eval("hello('HAW')");
} catch (final ScriptException se) { se.printStackTrace(); }

	
}



}
