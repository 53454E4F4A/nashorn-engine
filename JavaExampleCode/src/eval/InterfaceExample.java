package eval;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class InterfaceExample {
	
	public interface HelloInterface {
		String hello(String args);
	}
	
	public static void main(String[] args) throws Exception {
     
        ScriptEngineManager factory = new ScriptEngineManager();
    
        ScriptEngine engine = factory.getEngineByName("nashorn"); 
       
        try {
            engine.eval("function hello(args) { return 'hello ' + args + '!' };");  
            Invocable invocable = (Invocable) engine;
HelloInterface greetings 
	= invocable.getInterface(HelloInterface.class);
System.out.println(greetings.hello("HAW"));

            
        } catch (final ScriptException se) { se.printStackTrace(); }
    }
}

