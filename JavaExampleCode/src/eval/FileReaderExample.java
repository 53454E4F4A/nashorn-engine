package eval;


import java.io.FileReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class FileReaderExample {
	public static void main(String[] args) throws Exception {
        
        ScriptEngineManager factory = new ScriptEngineManager();
     
        ScriptEngine engine = factory.getEngineByName("nashorn"); 
        try {
engine.eval(new FileReader("/home/linnaeus/workspace/JavaScript Example Code/hello.js"));
        } catch (final ScriptException se) { se.printStackTrace(); }
    }
}