package JSObjects;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import eval.InterfaceExample.HelloInterface;

public class JSObjectExample {
	
public interface HelloInterface {
	void hello(String args);
	
}

    public static void main(String[] args) throws Exception {
    	ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

     
engine.eval("var instance = new Object()");
engine.eval("instance.hello = function(s) { print('Hallo '+ s + '!') }");
    
Object obj = engine.get("instance");
        
Invocable inv = (Invocable) engine;
   
inv.invokeMethod(obj, "hello", "HAW");

HelloInterface iHello = inv.getInterface(obj, HelloInterface.class);
iHello.hello("HAW");
     
    }
}
