package JSObjects;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import JSObjects.InnerClassExample.HelloInterface;

public class InnerClassExample {
	
	public interface HelloInterface {
		void hello(String args);
	}
	public class HelloClass implements HelloInterface {
		public String greeting = "hello ";
		public void hello(String s) {
			System.out.println(greeting + s + "!");
		}
		public void hello(double s) {
			System.out.println(s + "a" + s + "a" + s + "a" + " Batman!");
		}

	}
    public static void main(String[] args) throws Exception {
    	ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

 	   String script = "var outer = Java.type('UsingJavaClasses.InnerClassExample');\n"
 	       		+		"var inner = Java.type('UsingJavaClasses.InnerClassExample.HelloClass');\n"
 	       		+		"var instance_outer = new outer();\n"
 	       		+ 		"var instance = new inner(instance_outer);\n";
	 	engine.eval(script);
	 	
	 	
	 	engine.eval("instance['hello']('HAW')");
	 	engine.eval("instance['hello(String)']('HAW')");
	 	engine.eval("instance['hello(double)']('HAW')");   
        
 	    HelloInterface hi = (HelloInterface) engine.get("instance"); 	    
 	    hi.hello("HAW");
 	    
 	    HelloClass h = (HelloClass) engine.get("instance");
 	    System.out.println(h.greeting + "HAW!");
     
    }
}


