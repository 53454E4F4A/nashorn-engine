package JSObjects;
/*
 * @author jonas sch�ufler
 */

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class GetExample {
	ScriptEngineManager manager = new ScriptEngineManager();
	ScriptEngine engine = manager.getEngineByName("nashorn");

	public static void main(String[] args) throws Exception {

		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		try {
		
engine.eval("var i = 1");
engine.eval("var n = new java.lang.Integer(1)");
engine.eval("var a = 'JavaScriptString'");
engine.eval("var b = new String ('JavaScriptStringObject')");
engine.eval("var c = new java.lang.String ('JavaString')");
engine.eval("var o = {fh:'HAW', sg:new String('TI'), Jahr:2015}; ");
engine.eval("function hello() { this.s = 'ObjectAttribute';} ");
System.out.println("----------------------------------------------");
System.out.println("In JavaScript");
System.out.println("----------------------------------------------");

engine.eval("print('i :\t'  + ' : ' + typeof i + ' : getClass(): ' + i.getClass());"
		+ "print('n :\t' + ' : ' + typeof n+ ' : getClass(): ' + n.getClass());"
		+ "print('');"
		+ "print('a :\t'  + ' : ' +typeof a+ ' : getClass(): ' + a.getClass());"		
		+ "print('b :\t'  + ' : ' +typeof b);"
		+ "print('c :\t'  + ' : ' +typeof c+ ' : getClass(): ' + c.getClass());"
		+ "print('');"
		+ "print('o :\t' + ' : ' +typeof o);"
		+ "print ('containing');"
		+ "print('\t' + 'fh: '  + ' : ' +typeof o.fh);"
		+ "print('\t' + 'sg: ' + ' : ' +typeof o.sg);"
		+ "print('\t' + 'Jahr: '  + ' : ' +typeof o.Jahr);");



String out;
	out =  "i :\t" +": " + engine.get("i").getClass().getCanonicalName()  + "\n" +
		   "n :\t" +": " + engine.get("n").getClass().getCanonicalName()  + "\n\n"  +
		   "a :\t" +": " + engine.get("a").getClass().getCanonicalName() + "\n" +
		   "b :\t" + ": " + engine.get("b").getClass().getCanonicalName()  + " wrapping a " + ((jdk.nashorn.api.scripting.ScriptObjectMirror)engine.get("b")).getClassName() + "\n" +
		   "c :\t" +": " + engine.get("c").getClass().getCanonicalName() + "\n\n" +
		   "o :\t" + ": " + engine.get("o").getClass().getCanonicalName() +  " wrapping a " + ((jdk.nashorn.api.scripting.ScriptObjectMirror)engine.get("o")).getClassName() + "\n" +
		   "containing: \n" +
		   "\tfh:\t" +  ((jdk.nashorn.api.scripting.ScriptObjectMirror)engine.get("o")).getMember("fh").getClass().getCanonicalName() + "\n" +
		   "\tsg:\t" + ((jdk.nashorn.api.scripting.ScriptObjectMirror)engine.get("o")).getMember("sg").getClass().getCanonicalName() +"\n" +
		   "\tJahr\t" + ((jdk.nashorn.api.scripting.ScriptObjectMirror)engine.get("o")).getMember("Jahr").getClass().getCanonicalName() +"\n\n" +
	 engine.get("hello") + ": " + engine.get("hello").getClass().getCanonicalName() + " wrapping a " + ((jdk.nashorn.api.scripting.ScriptObjectMirror)engine.get("hello")).getClassName() 
	 + "\n" ;
	
System.out.println("----------------------------------------------");
System.out.println("In Java");
System.out.println("----------------------------------------------");
System.out.println(out);

			
		} catch (final ScriptException se) {
			se.printStackTrace();
		}

	}

}
