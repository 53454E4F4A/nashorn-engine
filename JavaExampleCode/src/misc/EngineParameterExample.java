package misc;

import javax.script.*;

import jdk.nashorn.api.scripting.*;

public class EngineParameterExample {
	public static void main(String[] args) throws ScriptException {
NashornScriptEngineFactory fac = new NashornScriptEngineFactory();
ScriptEngine engine = fac.getScriptEngine(new String[] {
	
	"--log=linking:finest",

});
		
		
		try {

			System.out.println("definition");
			engine.eval("function hello(args) " + "{ "
					+ "print('hello ' + args + '!') " + "};");
			System.out.println("string");
			engine.eval("hello('HAW')");
			System.out.println("number");
			engine.eval("hello(1)");
		} catch (final ScriptException se) {
			se.printStackTrace();
		}
	}
}