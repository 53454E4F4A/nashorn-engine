package misc;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

public class ListEngines {
  public static void main(String args[]) {
ScriptEngineManager manager = new ScriptEngineManager();
List<ScriptEngineFactory> factories = manager.getEngineFactories();  
for (ScriptEngineFactory factory : factories) {
  System.out.println("--------------------------");     
  System.out.println("Engine: " + factory.getEngineName() +" " + factory.getEngineVersion());
  System.out.println("Language: " + factory.getLanguageName() + " " + factory.getLanguageVersion() );
  System.out.println("Mime Types: " + factory.getMimeTypes());
  System.out.println("Names: "+ factory.getNames());
  }
}
}

