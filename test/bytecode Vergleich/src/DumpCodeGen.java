
import java.io.IOException;

import javax.script.*;

import jdk.nashorn.api.scripting.*;

public class DumpCodeGen {
	public static void main(String[] args) throws ScriptException, IOException {
		NashornScriptEngineFactory fac = new NashornScriptEngineFactory();
		ScriptEngine engine = fac.getScriptEngine(new String[] {
			//	"--print-code",
			//	"--stdout=out.txt",
				"--log=codegen:finest"
				});
//		ScriptEngine engine = fac.getScriptEngine(new String[] {
//			"-co",
//				"-d=test",
//				});
		try {
			// System.out.println(engine.getFactory().getEngineName());
			String script = "function hello(args) " + "{ "
								+ "print('hello ' + args + '!') "
							+ "};"
							+ "hello('HAW');" 
							+ "hello(1);" 
							+ "hello(1);";
			System.in.read();
			
			for(int i = 0; i < 10; i++) {
				System.out.println("Wiederhohlung " + i);
				engine.eval(script);
				System.in.read();
			}
			
		} catch (final ScriptException se) {
			se.printStackTrace();
		}
	}
}