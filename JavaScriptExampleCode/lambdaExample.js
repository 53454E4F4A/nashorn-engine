var TIntegerArray = Java.type("java.lang.Integer[]");
var intList = java.util.Arrays.asList(Java.to([1, 2, 3, 4, 5, 6, 7, 8, 9, 0], TIntegerArray));
intList.stream().filter(function(n) n%2).forEach(function(n) print(n))