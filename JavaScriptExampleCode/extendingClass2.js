var Base = Java.type("ExtendExampleClass")
var Extended = Java.extend(Base)
instance = new Extended() {
	greetings: function() {
		print("Greetings from " + instance.s);
	}
};
instance.setString("JavaScript");
instance.greetings();
base = Java.super(instance);
base.greetings();



