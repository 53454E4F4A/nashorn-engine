// Create a JavaScript array
var anArray = [1, "13", false];


var intArray = Java.to(anArray, "int[]");
var stringArray = Java.to(anArray, Java.type("Java.lang.String[]"));
var objectArray = Java.to(anArray);

print(IntArray[0]); //  1
print(IntArray[1]); //  13
print(IntArray[2]); // 0

print(StringArray[0]);  "1"
print(StringArray[1]);  "13"
print(StringArray[2]); "false"

print(ObjectArray[0]); //  1
print(ObjectArray[1]); // "13"
print(ObjectArray[2]); // false