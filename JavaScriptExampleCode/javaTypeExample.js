#!/usr/bin/jjs
var TIntArray = Java.type("int[]");
var TIntegerArray = Java.type("java.lang.Integer[]");
var CArrays = Java.type("java.util.Arrays");
var TArrayList = Java.type("java.util.ArrayList");

var intArray = Java.to([1, 2, 3, 4, 5, 6, 7, 8, 9, 0], TIntArray)
print("int[] array: " + intArray);
var intList = CArrays.asList(intArray);
print(intList.size() + " " + intList.get(0));
var intArray = Java.to([1, 2, 3, 4, 5, 6, 7, 8, 9, 0], TIntegerArray)
var intList = CArrays.asList(intArray);
print(intList.size());