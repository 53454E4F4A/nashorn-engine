var Base = Java.type("ExtendExampleClass")
var instance = new Base();
instance.greetings();

var Extended = Java.extend(Base, {
	greetings: function() {
		print("Greetings from JavaScript");
	}
});
instance = new Extended();
instance.greetings();
instance.setString("JavaScript");
base = Java.super(instance);
base.greetings();
print(base);
print(instance);

