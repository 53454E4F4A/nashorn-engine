var imports = new JavaImporter(java.util, java.lang);

with(imports) {
	var s = new String("JSString");
	var s2 = new java.lang.String("JavaString");	
	var i = new Integer(3);
	var hm = new HashMap()
	
	hm.put(1, s)
	hm.put("2", i)
	hm.put(s2, s)
	hm.entrySet().forEach(function(entry) print("Key: " + entry.getKey()+ " of type " + typeof(entry.getKey()) + 
			"\tValue: " + entry.getValue() + " of type " + typeof(entry.getValue())));
}