#!/usr/bin/jjs
files = java.util.Arrays.asList(`ls -la`.split("\n"));
print('found in ' + $ENV.PWD + ':');
files.stream().filter(
		function(s) return s.contains(new java.lang.String($ARG[0])
).forEach(function(s) print(s));