public class ExtendExampleClass {
	protected String s = "from Java";
	
	public void greetings() {
		System.out.println("Hello " + s);
	}
	
	public void setString(String s) { 
		this.s = s;
	}
	public String getString() { 
		return s;
	}
}