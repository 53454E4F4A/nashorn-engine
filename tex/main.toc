\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Projekt Nashorn}{2}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Aufbau der Arbeit}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Nashorn in Java}{3}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Die Klasse ScriptObjectMirror}{4}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Nashorn Java API}{6}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Java Typen in Nashorn}{6}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Die Java.extend Funktion}{9}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Anonyme Klassen und Lambda-Ausdr\"ucke}{10}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Nashorn in der Shell}{12}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}jrunscript und jjs}{12}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Die -scripting Option}{13}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Benchmarks}{15}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Octane Benchmarks}{15}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Der Benchmark Code}{16}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Rhino vs Nashorn}{17}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Fazit}{23}{chapter.6}
